package edu.ntnu.ericbi.wargames;

import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.units.*;
import junit.framework.TestCase;
import java.util.ArrayList;
import java.util.List;


public class ArmyTest extends TestCase {
    public void testGetName() {
        Army testArmy = new Army("TestName");
        assertEquals("TestName", testArmy.getName());
    }

    public void testAdd() {
        Army testArmy = new Army("TestName");
        Unit testUnit = new InfantryUnit("TestInfantry", 100);
        testArmy.add(testUnit);
        assertTrue(testArmy.getAllUnits().contains(testUnit));
    }

    public void testAddAll() {
        Army testArmy = new Army("TestName");
        Unit testUnit1 = new InfantryUnit("TestInfantry", 100);
        Unit testUnit2 = new InfantryUnit("TestInfantry", 100);

        ArrayList<Unit> list = new ArrayList<>();
        list.add(testUnit1);
        list.add(testUnit2);

        testArmy.addAll(list);
        List<Unit> allUnits = testArmy.getAllUnits();
        assertTrue(allUnits.contains(testUnit1) && allUnits.contains(testUnit2));
    }

    public void testRemove() {
        Army testArmy = new Army("TestName");
        Unit testUnit = new InfantryUnit("TestInfantry", 100);
        testArmy.add(testUnit);
        testArmy.remove(testUnit);
        assertFalse(testArmy.hasUnits());
    }

    public void testHasUnits() {
        Army testArmy = new Army("TestName");
        Unit testUnit = new InfantryUnit("TestInfantry", 100);
        testArmy.add(testUnit);
        assertTrue(testArmy.hasUnits());
    }

    public void testGetAllUnits() {
        Army testArmy = new Army("TestName");
        testArmy.add(new InfantryUnit("TestInfantry", 100));
        testArmy.add(new InfantryUnit("TestInfantry", 100));

        assertEquals(2, testArmy.getAllUnits().size());
    }

    public void testGetInfantryUnits() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        List<Unit> units = army.getInfantryUnits();
        assertEquals(10, units.size());
        assertTrue(units.get(0) instanceof InfantryUnit);
    }

    public void testGetRangedUnits() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        List<Unit> units = army.getRangedUnits();
        assertEquals(10, units.size());
        assertTrue(units.get(0) instanceof RangedUnit);
    }

    public void testGetCavalryUnits() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        List<Unit> units = army.getCavalryUnits();
        assertEquals(10, units.size());
        assertTrue(units.get(0) instanceof CavalryUnit);
    }

    public void testGetCommanderUnits() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        List<Unit> units = army.getCommanderUnits();
        assertEquals(10, units.size());
        assertTrue(units.get(0) instanceof CommanderUnit);
    }

    public void testGetRandom() {
        Army testArmy = new Army("TestName");
        testArmy.add(new InfantryUnit("TestInfantry", 100));

        assertNotNull(testArmy.getRandom());
    }

    public void testToString() {
        Army testArmy = new Army("TestName");

        assertEquals("TestName with 0 units.", testArmy.toString());
    }

    public void testEquals() {
        Army testArmy1 = new Army("TestName");
        testArmy1.add(new InfantryUnit("TestInfantry", 100));
        testArmy1.add(new InfantryUnit("TestInfantry", 100));

        Army testArmy2 = new Army("TestName");
        testArmy2.add(new InfantryUnit("TestInfantry", 100));
        testArmy2.add(new InfantryUnit("TestInfantry", 100));

        assertEquals(testArmy1, testArmy2);
    }
}
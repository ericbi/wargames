package edu.ntnu.ericbi.wargames.army.units;

import junit.framework.TestCase;


public class CommanderUnitTest extends TestCase {
    public void testInitiatingCommanderWorks() {
        try {
            new CommanderUnit("Test Unit", 100);
        } catch (Exception e) {
            fail("Failed to initialize a Commander unit. " + e.getMessage());
        }
    }

    public void testCommanderHasCorrectAttack() {
        CommanderUnit testUnit = new CommanderUnit("Test Unit", 100);
        assertEquals(CommanderUnit.DEFAULT_ATTACK_POWER, testUnit.getAttack());
    }

    public void testCommanderHasCorrectArmor() {
        CommanderUnit testUnit = new CommanderUnit("Test Unit", 100);
        assertEquals(CommanderUnit.DEFAULT_ARMOR, testUnit.getArmor());
    }
}
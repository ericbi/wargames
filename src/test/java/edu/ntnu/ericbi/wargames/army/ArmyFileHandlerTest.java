package edu.ntnu.ericbi.wargames.army;

import edu.ntnu.ericbi.wargames.army.units.CavalryUnit;
import edu.ntnu.ericbi.wargames.army.units.CommanderUnit;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;
import edu.ntnu.ericbi.wargames.army.units.RangedUnit;
import junit.framework.TestCase;
import org.junit.function.ThrowingRunnable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.assertThrows;

public class ArmyFileHandlerTest extends TestCase {
    public void testSavingArmyToFileWorks() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        try {
            ArmyFileHandler.saveToFile(army, "src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/test.csv");
        } catch (IOException e) {
            e.printStackTrace();
            fail("Failed to write army to a file.");
        }

        File testFile = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/test.csv");
        assertTrue(testFile.exists());
    }


    // TODO: Rename all other methods to follow this naming convention, or vice versa
    public void testLoadingArmyFromFile_WithTooFewArguments_ThrowsIOException() throws IOException {
        File directoryPath = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles");

        // If the directory does not already exists and it fails in creating a new directory...
        if (!directoryPath.exists() && !directoryPath.mkdirs()) {
            throw new IOException(
                    "The directory to the file could not be created. Have you " +
                            "closed all other programs accessing the folder?" +
                            "\nAttempted to create this directory: " + directoryPath);
        }

        File file = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/tooFewArgs.army");
        file.createNewFile();

        FileWriter writer = new FileWriter("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/tooFewArgs.army");
        writer.write("Malformed Army\nInfantryUnit\n");
        writer.close();

        assertThrows(
                IOException.class,
                () -> ArmyFileHandler.loadArmyFromFile(file)
        );
    }

    // TODO: Rename all other methods to follow this naming convention, or vice versa
    public void testLoadingArmy_WithTooManyArguments_ThrowsIOException() throws IOException {
        File directoryPath = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles");
        // If the directory does not already exists and it fails in creating a new directory...
        if (!directoryPath.exists() && !directoryPath.mkdirs()) {
            throw new IOException(
                    "The directory to the file could not be created. Have you closed " +
                            "all other programs accessing the folder?" +
                            "\nAttempted to create this directory: " + directoryPath);
        }

        FileWriter writer = new FileWriter("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/tooManyArgs.army");
        writer.write("Malformed Army\nInfantryUnit, 1, 2, 3, 4, 5\n");
        writer.close();

        File file = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/tooManyArgs.army");
        assertThrows(
                IOException.class,
                () -> ArmyFileHandler.loadArmyFromFile(file)
        );
    }

    // TODO: Rename all other methods to follow this naming convention, or vice versa
    public void testLoadingArmyFromFile_WithWrongArgumentOrder_ThrowsNumberFormatException() throws IOException {
        File file = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/wrongArgOrder.army");
        File directoryPath = new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles");

        // If the directory does not already exists and it fails in creating a new directory...
        if (!directoryPath.exists() && !directoryPath.mkdirs()) {
            throw new IOException(
                    "The directory to the file could not be created. Have you" +
                            " closed all other programs accessing the folder?" +
                            "\nAttempted to create this directory: " + directoryPath);
        }

        file.createNewFile();

        FileWriter writer = new FileWriter(file);
        writer.write("Malformed Army\n9, 10, InfantryUnit\n");
        writer.close();

        assertThrows(
                NumberFormatException.class,
                () -> ArmyFileHandler.loadArmyFromFile(file)
        );
    }


    public void testLoadingArmyFromFile() {
        Army army = new Army("Test Army");

        for (int i = 0; i < 10; i++) {
            army.add(new InfantryUnit("Infantry", 10));
            army.add(new RangedUnit("Ranged", 12));
            army.add(new CavalryUnit("Cavalry", 14));
            army.add(new CommanderUnit("Commander", 16));
        }

        String pathToFile = "src/test/java/edu/ntnu/ericbi/wargames/temp-files/armyfiles/test.csv";

        try {
            ArmyFileHandler.saveToFile(army, pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Failed to write army to a file.");
        }

        try {
            Army readArmy = ArmyFileHandler.loadArmyFromFile(new File(pathToFile));
            assertEquals(readArmy, army);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Could not read army from file.");
        }
    }

    private static void deleteDirectory(File directoryToBeDeleted) throws IOException {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null && allContents.length != 0) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        Files.delete(directoryToBeDeleted.toPath());
    }

    @Override
    protected void tearDown() throws Exception {
        deleteDirectory(new File("src/test/java/edu/ntnu/ericbi/wargames/temp-files"));
        super.tearDown();
    }
}
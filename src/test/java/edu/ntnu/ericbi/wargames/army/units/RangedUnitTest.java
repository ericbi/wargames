package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import junit.framework.TestCase;


public class RangedUnitTest extends TestCase {
    public void testInitiatingRangedWorks() {
        try {
            new RangedUnit("Test", 100);
        } catch (Exception e) {
            fail("Failed to initialize a Ranged unit. " + e.getMessage());
        }
    }

    public void testAttackBonusIsCorrect() {
        RangedUnit testUnit = new RangedUnit("Test", 100);

        // The attack bonus should be constant
        assertEquals(RangedUnit.DEFAULT_ATTACK_BONUS, testUnit.getAttackBonus());
    }

    public void testResistBonusWhenTimesTakenDamageIsZero() {
        RangedUnit defender = new RangedUnit("Test", 100);

        assertEquals(RangedUnit.STARTING_RESIST_BONUS, defender.getResistBonus());
    }

    public void testResistBonusWhenTimesTakenDamageIsOne() {
        RangedUnit attacker = new RangedUnit("Test", 100);
        RangedUnit defender = new RangedUnit("Test", 100);

        attacker.attack(defender, TerrainType.PLAINS);
        assertEquals(RangedUnit.STARTING_RESIST_BONUS - RangedUnit.RESIST_BONUS_DECAY_AMOUNT, defender.getResistBonus());
    }


    public void testResistBonusWhenTimesTakenDamageIsTwo() {
        RangedUnit attacker = new RangedUnit("Test", 100);
        RangedUnit defender = new RangedUnit("Test", 100);

        attacker.attack(defender, TerrainType.PLAINS);
        attacker.attack(defender, TerrainType.PLAINS);
        assertEquals(RangedUnit.STARTING_RESIST_BONUS - 2*RangedUnit.RESIST_BONUS_DECAY_AMOUNT, defender.getResistBonus());
    }

    public void testResistBonusStaysAtBaselineAfterManyAttacks () {
        RangedUnit attacker = new RangedUnit("Test", 10000);
        RangedUnit defender = new RangedUnit("Test", 10000);

        attacker.attack(defender, TerrainType.PLAINS);
        attacker.attack(defender, TerrainType.PLAINS);
        attacker.attack(defender, TerrainType.PLAINS);
        attacker.attack(defender, TerrainType.PLAINS);
        attacker.attack(defender, TerrainType.PLAINS);

        assertEquals(RangedUnit.BASELINE_RESIST_BONUS, defender.getResistBonus());
    }

    // TODO: Test copy method
}
package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import junit.framework.TestCase;


public class InfantryUnitTest extends TestCase {
    public void testGetAttackBonus() {
        InfantryUnit unit = new InfantryUnit("Infantry", 100);

        // Infantry unit's attack bonus doesn't change over time, so they should be the same
        assertEquals(InfantryUnit.DEFAULT_ATTACK_BONUS, unit.getAttackBonus());
    }

    public void testGetResistBonus() {
        InfantryUnit unit = new InfantryUnit("Infantry", 100);

        // Infantry unit's resist bonus doesn't change over time, so they should be the same
        assertEquals(InfantryUnit.DEFAULT_RESIST_BONUS, unit.getResistBonus());
    }

    public void testInitiatingInfantryWorks() {
        try {
            new InfantryUnit("An infantry unit", 100);
        } catch (Exception e) {
            fail("Failed to initialize a generic unit. " + e.getMessage());
        }
    }

    public void testAttackBonusDoesNotChangeAfterAttackingOrTakingDamage() {
        InfantryUnit attacker = new InfantryUnit("Test Unit", 100);
        InfantryUnit opponent = new InfantryUnit("Defender Unit", 100);

        // Random attacks
        attacker.attack(opponent, TerrainType.FOREST);
        attacker.attack(opponent, TerrainType.FOREST);
        opponent.attack(attacker, TerrainType.FOREST);

        // Attack bonus should not change for infantry
        assertEquals(InfantryUnit.DEFAULT_ATTACK_BONUS, attacker.getAttackBonus());
    }

    public void testResistBonusDoesNotChangeAfterAttackingOrTakingDamage() {
        InfantryUnit attacker = new InfantryUnit("Test Unit", 100);
        InfantryUnit opponent = new InfantryUnit("Defender unit", 100);

        // Random attacks
        attacker.attack(opponent, TerrainType.FOREST);
        attacker.attack(opponent, TerrainType.FOREST);
        opponent.attack(attacker, TerrainType.FOREST);

        // Resist bonus should not change for infantry
        assertEquals(InfantryUnit.DEFAULT_RESIST_BONUS, attacker.getResistBonus());
    }

    // TODO: Test copy method
}
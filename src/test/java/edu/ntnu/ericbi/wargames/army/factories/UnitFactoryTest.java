package edu.ntnu.ericbi.wargames.army.factories;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.UnitType;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import static edu.ntnu.ericbi.wargames.army.factories.UnitFactory.createUnit;
import static edu.ntnu.ericbi.wargames.army.factories.UnitFactory.createUnits;
import static org.junit.Assert.assertThrows;

public class UnitFactoryTest extends TestCase {
    public void testCreateUnitWorks() {
        Unit soldier1 = new InfantryUnit("Test", 100);
        Unit soldier2 = createUnit(UnitType.InfantryUnit, "Test", 100);

        assertEquals(soldier1, soldier2);
    }

    public void testCreateUnitsWorks() {
        List<Unit> units1 = new ArrayList<>();
        units1.add(new InfantryUnit("Test", 100));
        units1.add(new InfantryUnit("Test", 100));

        List<Unit> units2 = createUnits(2, UnitType.InfantryUnit, "Test", 100);
        assertEquals(units1, units2);
    }

    public void testCreateUnitsWhenAmountLowerThanZeroThrowsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> createUnits(-10, UnitType.InfantryUnit, "Test", 100)
        );
    }
}
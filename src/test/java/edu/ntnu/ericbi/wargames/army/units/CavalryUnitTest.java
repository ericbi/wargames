package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import junit.framework.TestCase;


public class CavalryUnitTest extends TestCase {
    public void testGetResistBonus() {
        CavalryUnit attacker = new CavalryUnit("Attacker", 100);

        // Resist bonus is constant
        assertEquals(CavalryUnit.DEFAULT_RESIST_BONUS, attacker.getResistBonus());
    }

    public void testInitiatingCavalryWorks() {
        try {
            new CavalryUnit("Test Unit", 100);
        } catch (Exception e) {
            fail("Failed to initialize a Cavalry unit. " + e.getMessage());
        }
    }

    public void testFirstDamageDealsExtraDamage() {
        CavalryUnit attacker = new CavalryUnit("Attacker", 100);

        // Create a unit with 0 armor and resist bonus for easier damage calculation
        Unit defender = new Unit("Defender", 0, 0, 100) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        int expectedDamage = CavalryUnit.DEFAULT_ATTACK_POWER +
                             CavalryUnit.CHARGE_BONUS +
                             CavalryUnit.DEFAULT_ATTACK_BONUS +
                             attacker.getTerrainAttackBonus(TerrainType.FOREST);

        attacker.attack(defender, TerrainType.FOREST);
        assertEquals(expectedDamage, 100-defender.getHealth());
    }

    public void testSecondDamageDealsBaseDamage() {
        CavalryUnit attacker = new CavalryUnit("Attacker", 100);

        // Create a unit with 0 armor and resist bonus for easier damage calculation
        Unit defender = new Unit("Defender", 0, 0, 100) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        int firstAttack = CavalryUnit.CHARGE_BONUS +
                          CavalryUnit.DEFAULT_ATTACK_POWER +
                          CavalryUnit.DEFAULT_ATTACK_BONUS +
                          attacker.getTerrainAttackBonus(TerrainType.FOREST);

        int secondAttack = CavalryUnit.DEFAULT_ATTACK_POWER +
                           CavalryUnit.DEFAULT_ATTACK_BONUS +
                           attacker.getTerrainAttackBonus(TerrainType.FOREST);

        attacker.attack(defender, TerrainType.FOREST);
        attacker.attack(defender, TerrainType.FOREST);
        assertEquals(firstAttack + secondAttack, 100-defender.getHealth());
    }

    public void testHasAttackedStartAsFalse() {
        CavalryUnit attacker = new CavalryUnit("Attacker", 100);
        assertFalse(attacker.hasAttacked);
    }

    public void testAttackingSetsHasAttackedToTrue() {
        CavalryUnit attacker = new CavalryUnit("Attacker", 100);
        CavalryUnit defender = new CavalryUnit("Defender", 100);

        attacker.attack(defender, TerrainType.FOREST);
        assertTrue(attacker.hasAttacked);
    }

    // TODO: Test copy method
}
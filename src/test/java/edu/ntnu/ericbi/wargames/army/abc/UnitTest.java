package edu.ntnu.ericbi.wargames.army.abc;

import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import edu.ntnu.ericbi.wargames.exceptions.UnitNotImplementedException;
import edu.ntnu.ericbi.wargames.army.units.*;
import junit.framework.TestCase;
import static org.junit.Assert.*;

// ---------------------------------------- NOTE ----------------------------------------------
// JavaDoc is intentionally left out for tests here, in favour of self-explanatory method names
// --------------------------------------------------------------------------------------------

public class UnitTest extends TestCase {
    public void testInitiatingGenericUnitWorks() {
        try {
            new Unit("TestUnit", 5, 3, 10) {
                public int getAttackBonus() { return 4; }
                public int getResistBonus() { return 0; }
                public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
                public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
                public Unit copy() {
                    return null;
                }
            };
        } catch (Exception e) {
            fail("Failed to initialize a generic unit. " + e.getMessage());
        }
    }

    public void testInitiatingGenericUnitWithTakeDamageOverriddenWorks() {
        try {
            new Unit("TestUnit", 5, 3, 10) {
                @Override
                public void takeDamage(int damage) {
                    System.out.println("Ouch!");
                }
                public int getAttackBonus() { return 4; }
                public int getResistBonus() { return 0; }
                public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
                public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
                public Unit copy() {
                    return null;
                }
            };
        } catch (Exception e) {
            fail("Failed to create a generic unit with overridden takeDamage method.");
        }
    }

    public void testInitiatingWithZeroHealthDoesNotThrowException() {
        // Only negative health is illegal. Therefore, 0 health should be allowed.
        new Unit("TestUnit", 0, 0, 0) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
    }

    public void testInitiatingUnitWithNegativeHealthThrowsException() {
        try {
            new Unit("TestUnit", 0, 0, -10) {
                public int getAttackBonus() { return 0; }
                public int getResistBonus() { return 0; }
                public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
                public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
                public Unit copy() {
                    return null;
                }
            };
            fail("Created a unit with negative health without throwing any exception.");
        } catch (IllegalArgumentException e) {
            assertEquals("Health can not be less than 0.", e.getMessage());
        } catch (Exception e) {
            fail(String.format("Got an unexpected exceptions: %s", e.getMessage()));
        }
    }

    public void testGetName() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        assertEquals("TestUnit", testUnit.getName());
    }

    public void testGetAttack() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        assertEquals(1, testUnit.getAttack());
    }

    public void testGetArmor() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        assertEquals(3, testUnit.getArmor());
    }

    public void testGetHealth() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        assertEquals(10, testUnit.getHealth());
    }

    public void testSetHealth() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        testUnit.setHealth(5);
        assertEquals(5, testUnit.getHealth());
    }

    public void testSetHealthToInvalidHealthThrowsException() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        try {
            testUnit.setHealth(-2);
            fail("Setting health to a negative number did not throw an exception.");
        } catch (IllegalArgumentException e) {
            assertEquals("Health value can not be set to less than 0.", e.getMessage());
        } catch (Exception e) {
            fail(String.format("Got an unexpected exception: %s", e.getMessage()));
        }
    }

    public void testToString() {
        Unit testUnit = new Unit("TestUnit", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        assertEquals(
                String.format(
                        "TestUnit (%s) has 10 health, 1 (+4) attack and 3 (+0) armor.",
                        testUnit.getClass().getSimpleName()
                ),
                testUnit.toString()
        );
    }

    public void testAttackDealsDamageToDefender() {
        Unit attacker = new Unit("Attacker", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        Unit defender = new Unit("Defender", 2, 2, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 2; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        // Attack once
        attacker.attack(defender, TerrainType.FOREST);
        assertEquals(9, defender.getHealth());
    }

    public void testAttackingTwiceDealsDamageCorrectly() {
        Unit attacker = new Unit("Attacker", 1, 3, 10) {
            public int getAttackBonus() { return 4; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        Unit defender = new Unit("Defender", 2, 2, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 2; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        // Attack twice
        attacker.attack(defender, TerrainType.FOREST);
        attacker.attack(defender, TerrainType.FOREST);

        assertEquals(8, defender.getHealth());
    }

    public void testAttackingWithNegativeDamageDoesNotHeal() {
        Unit attacker = new Unit("Attacker", 0, 0, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };
        Unit defender = new Unit("Defender", 0, 0, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 5; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        // Damage should be -5, which should be clamped to 0.
        attacker.attack(defender, TerrainType.FOREST);
        assertEquals(10, defender.getHealth());
    }

    public void testCreatingInfantryFromString() throws UnitNotImplementedException {
        InfantryUnit unit = (InfantryUnit) Unit.createUnitFromString("InfantryUnit", "Test", 10);
        InfantryUnit expectedUnit = new InfantryUnit("Test", 10);

        assertEquals(expectedUnit, unit);
    }

    public void testCreatingRangedFromString() throws UnitNotImplementedException {
        RangedUnit unit = (RangedUnit) Unit.createUnitFromString("RangedUnit", "Test", 10);
        RangedUnit expectedUnit = new RangedUnit("Test", 10);

        assertEquals(expectedUnit, unit);
    }

    public void testCreatingCavalryFromString() throws UnitNotImplementedException {
        CavalryUnit unit = (CavalryUnit) Unit.createUnitFromString("CavalryUnit", "Test", 10);
        CavalryUnit expectedUnit = new CavalryUnit("Test", 10);

        assertEquals(expectedUnit, unit);
    }

    public void testCreatingCommanderFromString() throws UnitNotImplementedException {
        CommanderUnit unit = (CommanderUnit) Unit.createUnitFromString("CommanderUnit", "Test", 10);
        CommanderUnit expectedUnit = new CommanderUnit("Test", 10);

        assertEquals(expectedUnit, unit);
    }

    public void testCreatingInvalidUnitFromString() {
        assertThrows(
                UnitNotImplementedException.class,
                () -> Unit.createUnitFromString("InvalidUnit", "Test", 10)
        );
    }
}
package edu.ntnu.ericbi.wargames;

import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import edu.ntnu.ericbi.wargames.exceptions.EmptyArmyException;
import junit.framework.TestCase;

import static org.junit.Assert.assertThrows;


public class BattleTest extends TestCase {
    public void testInitiatingBattleWorks() throws EmptyArmyException {
        Unit superBoss = new Unit("SuperBoss", 10, 10, 10000) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 100; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        Unit weakling = new Unit("Weakling", 1, 1, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return null;
            }
        };

        Army army1 = new Army("Test1");
        army1.add(superBoss);

        Army army2 = new Army("Test2");
        army2.add(weakling);

        new Battle(army1, army2, TerrainType.FOREST);
    }


    public void testSimulateEndsWithCorrectWinner() throws EmptyArmyException {
        Unit superBoss = new Unit("SuperBoss", 10, 10, 10000) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 100; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return this;
            }
        };

        Unit weakling = new Unit("Weakling", 1, 1, 10) {
            public int getAttackBonus() { return 0; }
            public int getResistBonus() { return 0; }
            public int getTerrainAttackBonus(TerrainType terrain) { return 0; }
            public int getTerrainDefenseBonus(TerrainType terrain) { return 0; }
            public Unit copy() {
                return this;
            }
        };

        Army army1 = new Army("Test1");
        army1.add(superBoss);

        Army army2 = new Army("Test2");
        army2.add(weakling);

        Battle battle = new Battle(army1, army2, TerrainType.FOREST);
        assertEquals(army1, battle.simulate());
    }


    public void testSimulateWithTwoEmptyArmiesThrowsIllegalStateException() throws EmptyArmyException {
        Army army1 = new Army("Test1");
        Army army2 = new Army("Test2");
        Battle battle = new Battle(army1, army2, TerrainType.FOREST);

        assertThrows( IllegalStateException.class, battle::simulate );
    }
}
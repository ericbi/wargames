package edu.ntnu.ericbi.wargames.army;


import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.units.CavalryUnit;
import edu.ntnu.ericbi.wargames.army.units.CommanderUnit;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;
import edu.ntnu.ericbi.wargames.army.units.RangedUnit;
import edu.ntnu.ericbi.wargames.exceptions.EmptyArmyException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A class representing an army with units. An army has a name and consists of units of type {@link Unit} that are
 * stored in a {@link List}.
 */
public class Army {
    /** Name of the army */
    private final String name;

    /** List of units contained by the army */
    private final List<Unit> units;

    /** Army constructor with empty unit list */
    public Army(String name) {
        if (name == null) throw new NullPointerException("The name of the army can not be null.");
        this.name = name;
        this.units = new ArrayList<>();
    }

    /** Army constructor from given units */
    public Army(String name, List<Unit> units) {
        if (name == null) throw new NullPointerException("The name of the army can not be null.");
        this.name = name;
        this.units = new ArrayList<>(units.stream().map(Unit::copy).collect(Collectors.toList()));
    }

    /**
     * Returns the name of the army.
     *
     * @return name of the army
     */
    public String getName() {
        return this.name;
    }

    /**
     * Add a unit to the Army's unit list.
     *
     * @param unit  The unit to add to the army.
     */
    public void add(Unit unit) {
        this.units.add(unit);
    }

    /**
     * Add all the given units to the Army's unit list.
     *
     * @param units  The units to add to the army.
     */
    public void addAll(List<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * Remove the given unit from the army.
     *
     * @param unit  The unit to remove from the army.
     */
    public void remove(Unit unit) {
        // I would want to return a true/false for whether the unit was removed or not, for consistency, however
        // since the task specified that this method returns void, and it's not a breaking change, I'll let it go.
        this.units.remove(unit);
    }

    /**
     * Check if the army contains any units.
     *
     * @return whether the army has units or not.
     */
    public boolean hasUnits() {
        return this.units.size() > 0;
    }

    /**
     * Get a shallow copy of all units in the army.
     *
     * @return a shallow copy of all units in the army.
     */
    public List<Unit> getAllUnits() {
        // Shallow copy
        return new ArrayList<>(this.units);
    }

    /**
     * Get a shallow copy of all Infantry units in the army.
     *
     * @return a shallow copy of all Infantry units in the army.
     */
    public List<Unit> getInfantryUnits() {
        List<InfantryUnit> units = this.units.stream().filter(unit -> unit.getClass() == InfantryUnit.class)
                                                      .map(InfantryUnit.class::cast)
                                                      .toList();
        return new ArrayList<>(units);
    }

    /**
     * Get a shallow copy of all Cavalry units in the army.
     *
     * @return a shallow copy of all Cavalry units in the army.
     */
    public List<Unit> getCavalryUnits() {
        List<CavalryUnit> units = this.units.stream().filter(unit -> unit.getClass() == CavalryUnit.class)
                                                     .map(CavalryUnit.class::cast)
                                                     .toList();
        return new ArrayList<>(units);
    }

    /**
     * Get a shallow copy of all Cavalry units in the army.
     *
     * @return a shallow copy of all Cavalry units in the army.
     */
    public List<Unit> getCommanderUnits() {
        List<CommanderUnit> units = this.units.stream().filter(unit -> unit.getClass() == CommanderUnit.class)
                                                       .map(CommanderUnit.class::cast)
                                                       .toList();
        return new ArrayList<>(units);
    }

    /**
     * Get a shallow copy of all Cavalry units in the army.
     *
     * @return a shallow copy of all Cavalry units in the army.
     */
    public List<Unit> getRangedUnits() {
        List<RangedUnit> units = this.units.stream().filter(unit -> unit.getClass() == RangedUnit.class)
                                                    .map(RangedUnit.class::cast)
                                                    .toList();
        return new ArrayList<>(units);
    }

    /**
     * Get a random unit from the army.
     *
     * @return a random unit from the army.
     */
    public Unit getRandom() {
        Random rng = new Random();
        int randomIndex = rng.nextInt(this.units.size());
        return this.units.get(randomIndex);
    }

    /**
     * Get total health of the army.
     *
     * @return the total health of the army.
     */
    public int getTotalHealth() {
        if (units.size() == 0) return 0;
        return this.units.stream().mapToInt(Unit::getHealth).sum();
    }

    /**
     * Get the average attack power of the army units.
     *
     * @return the average attack power of the army units.
     */
    public double getAverageAttack() {
        if (units.size() == 0) return 0;
        return units.stream().mapToInt(Unit::getAttack).sum() / (double) units.size();
    }

    /**
     * Get the average armor of the army units.
     *
     * @return the average armor of the army units.
     */
    public double getAverageArmor() {
        if (units.size() == 0) return 0;
        return units.stream().mapToInt(Unit::getArmor).sum() / (double) units.size();
    }

    @Override
    public String toString() {
        return String.format("%s with %s units.", this.name, this.units.size());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(getName(), army.getName()) && units.equals(army.getAllUnits());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), units);
    }
}

package edu.ntnu.ericbi.wargames.army.enums;

/**
 * An enum containing every type of terrain and their corresponding image path
 * relative to resources folder.
 */
public enum TerrainType {
    HILL("/gui/media/terrains/hill.png"),
    PLAINS("/gui/media/terrains/plains.png"),
    FOREST("/gui/media/terrains/forest.png");

    public final String imgUrl;
    TerrainType(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}

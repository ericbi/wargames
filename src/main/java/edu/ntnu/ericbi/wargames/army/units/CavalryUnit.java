package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;

/** A Cavalry army unit. */
public class CavalryUnit extends Unit {
    /** Default attack power */
    final static protected int DEFAULT_ATTACK_POWER = 20;

    /** Default armor */
    final static protected int DEFAULT_ARMOR = 12;

    /** Resist bonus */
    final static protected int DEFAULT_RESIST_BONUS = 2;

    /** Charge bonus that gets added to default attack bonus */
    final static protected int CHARGE_BONUS = 4;

    /** Default attack bonus, when the charge bonus is not in use */
    final static protected int DEFAULT_ATTACK_BONUS = 2;

    /** Whether the unit has attacked yet or not */
    protected boolean hasAttacked;


    /**
     * Base constructor for a Cavalry unit.
     *
     * @param name  the name of the Cavalry unit
     * @param health  the name of the Cavalry unit
     */
    public CavalryUnit(String name, int health) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
        hasAttacked = false;
    }

    /**
     * A constructor for making copies of this unit.
     *
     * @param name  The name of the unit.
     * @param health  The health of the unit.
     * @param startingHasAttacked  If the unit should start with having attacked or not.
     */
    public CavalryUnit(String name, int health, boolean startingHasAttacked) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
        hasAttacked = startingHasAttacked;
    }

    /**
     * Constructor for child classes of Cavalry to use when creating different
     * types of cavalry units, with different attack and armor stats.
     *
     * @param name  the name of the cavalry unit
     * @param attack  the attack of the cavalry unit
     * @param armor  the armor of the cavalry unit
     * @param health  the health of the cavalry unit
     */
    // I've chosen to make this constructor protected instead of public like the UML diagram in
    // the exercise shows. This is because new units should be created like the CommanderUnit;
    // as a new class that inherits from Cavalry, instead of a Cavalry class with different stats.
    // Therefore, this constructor will only be used in classes that inherit from Cavalry.
    protected CavalryUnit(String name, int attack, int armor, int health) {
        super(name, attack, armor, health);
        hasAttacked = false;
    }

    @Override
    public int getAttackBonus() {
        // If the cavalry unit has not attacked, it charges and gets a 6 attack bonus instead.
        return DEFAULT_ATTACK_BONUS + (hasAttacked ? 0 : CHARGE_BONUS);
    }

    @Override
    public int getTerrainAttackBonus(TerrainType terrain) {
        if (terrain.equals(TerrainType.PLAINS)) return 2;
        return 0;
    }

    @Override
    public int getResistBonus() {
        return DEFAULT_RESIST_BONUS;
    }

    @Override
    public int getTerrainDefenseBonus(TerrainType terrain) {
        if (terrain.equals(TerrainType.FOREST)) return -getResistBonus();
        return 0;
    }

    @Override
    public Unit copy() {
        return new CavalryUnit(getName(), getHealth(), hasAttacked);
    }

    @Override
    public void attack(Unit opponent, TerrainType terrain) {
        super.attack(opponent, terrain);
        if (!hasAttacked) hasAttacked = true;
    }
}

package edu.ntnu.ericbi.wargames.army.factories;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.UnitType;
import edu.ntnu.ericbi.wargames.army.units.CavalryUnit;
import edu.ntnu.ericbi.wargames.army.units.CommanderUnit;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;
import edu.ntnu.ericbi.wargames.army.units.RangedUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * A factory for creating units given a {@link UnitType}. This class contains methods
 * for easily generating one or many instances of a given unit.
 */
public class UnitFactory {
    /**
     * Create a unit given a {@link UnitType}.
     *
     * @param unitType  Unit type to create an instance of.
     * @param name  Unit name.
     * @param health  Unit health.
     * @return the created instance of the requested unit.
     */
    public static Unit createUnit(UnitType unitType, String name, int health) {
        return switch (unitType) {
            case InfantryUnit -> new InfantryUnit(name, health);
            case RangedUnit -> new RangedUnit(name, health);
            case CavalryUnit -> new CavalryUnit(name, health);
            case CommanderUnit -> new CommanderUnit(name, health);
        };
    }

    /**
     * Create multiple units given a {@link UnitType}.
     *
     * @param amount  The amount of units to create.
     * @param unitType  Unit type to create an instance of.
     * @param name  Unit name.
     * @param health  Unit health.
     * @return an ArrayList of instances of the requested unit type.
     */
    public static List<Unit> createUnits(int amount, UnitType unitType, String name, int health) {
        if (amount < 0)
            throw new IllegalArgumentException("Cannot create a negative amount of units.");

        List <Unit> units = new ArrayList<>();

        // Chose to use a switch statement here instead of calling the createUnit
        // function i amount of times. This is for performance reasons so that
        // the switch statement does not need to be run every time. This pattern
        // does not sacrifice readability a lot either, so I would say it is ok.
        switch (unitType) {
            case InfantryUnit:
                for (int i = 0; i < amount; i++) units.add(new InfantryUnit(name, health));
                break;

            case RangedUnit:
                for (int i = 0; i < amount; i++) units.add(new RangedUnit(name, health));
                break;

            case CavalryUnit:
                for (int i = 0; i < amount; i++) units.add(new CavalryUnit(name, health));
                break;

            case CommanderUnit:
                for (int i = 0; i < amount; i++) units.add(new CommanderUnit(name, health));
                break;
        }

        return units;
    }
}

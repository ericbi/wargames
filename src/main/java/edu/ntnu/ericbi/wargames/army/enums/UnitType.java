package edu.ntnu.ericbi.wargames.army.enums;

/**
 * Enum containing every unit type.
 */
public enum UnitType {
    CavalryUnit,
    CommanderUnit,
    InfantryUnit,
    RangedUnit
}

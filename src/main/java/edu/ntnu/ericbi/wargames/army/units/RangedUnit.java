package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;

/** A Ranged army unit. */
public class RangedUnit extends Unit {
    /** Amount of times the ranged unit has taken damage */
    protected int timesTakenDamage;

    /** Default attack power */
    final static protected int DEFAULT_ATTACK_POWER = 15;

    /** Default armor */
    final static protected int DEFAULT_ARMOR = 8;

    /** Attack bonus */
    final static protected int DEFAULT_ATTACK_BONUS = 3;

    /** Resist bonus the unit starts with */
    final static protected int STARTING_RESIST_BONUS = 6;

    /** The amount the resist bonus is decreased by after each time taking damage */
    final static protected int RESIST_BONUS_DECAY_AMOUNT = 2;

    /** The baseline resist bonus / the lowest resist bonus the ranged unit can have */
    final static protected int BASELINE_RESIST_BONUS = 2;

    /**
     * Base constructor for a Ranged unit.
     *
     * @param name  The name of the ranged unit
     * @param health  The health of the ranged unit
     */
    public RangedUnit(String name, int health) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
        timesTakenDamage = 0;
    }

    /**
     * Constructor used for creating a copy of the unit.
     *
     * @param name  The name of the unit.
     * @param health  The health of the unit.
     * @param startingTimesTakenDamage  The amount of times the unit starts with having taken damage.
     */
    private RangedUnit(String name, int health, int startingTimesTakenDamage) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
        timesTakenDamage = startingTimesTakenDamage;
    }

    @Override
    public void takeDamage(int damage) {
        super.takeDamage(damage);
        this.timesTakenDamage++;
    }

    @Override
    public int getAttackBonus() {
        return DEFAULT_ATTACK_BONUS;
    }

    @Override
    public int getTerrainAttackBonus(TerrainType terrain) {
        return switch (terrain) {
            case HILL -> 2;
            case FOREST -> -1;
            case PLAINS -> 0;
        };
    }

    /**
     * Get the resist bonus for the unit.
     *
     * @return adjusted resist bonus based on the amount of times the unit has been attacked
     */
    @Override
    public int getResistBonus() {
        return Integer.max(BASELINE_RESIST_BONUS, STARTING_RESIST_BONUS - timesTakenDamage * RESIST_BONUS_DECAY_AMOUNT);
    }

    @Override
    public int getTerrainDefenseBonus(TerrainType terrain) {
        return 0;
    }

    @Override
    public Unit copy() {
        return new RangedUnit(getName(), getHealth(), timesTakenDamage);
    }
}

package edu.ntnu.ericbi.wargames.army;

import edu.ntnu.ericbi.wargames.GUI;
import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;
import javafx.scene.control.Alert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * File related operations for the Army class. This class contains for example a method
 * to create an Army object from a saved army file.
 */
public class ArmyFileHandler {
    private static final String armyPath = "/armies/";

    /**
     * Load an Army from a file that exists in the default army file folder.
     *
     * @param fileName  The file name, e.g. "humans.army" or "my-orcish-army.army".
     *
     * @return an Army instance.
     * @throws URISyntaxException if there is an error with the fileName or file structure.
     * @throws IOException if the file is empty, or malformed data is present.
     * @throws FileNotFoundException if the file is not found.
     */
    public static Army loadArmy(String fileName) throws URISyntaxException, IOException {
        URI fileURI;
        try {
            fileURI = ArmyFileHandler.class.getResource(armyPath + fileName).toURI();
        } catch (URISyntaxException e) {
            String errorMsg = "Wrong URI syntax at index " + e.getIndex() + " when loading Army from file:\n" + armyPath + fileName + "\n\n" + e.getStackTrace();
            GUI.showAlert(Alert.AlertType.ERROR, errorMsg);
            throw new URISyntaxException(armyPath + fileName, errorMsg, e.getIndex());
        }

        try {
            return loadArmyFromFile(new File(fileURI));
        } catch (IOException e) {
            GUI.showAlert(Alert.AlertType.ERROR, e.getStackTrace().toString());
            throw e;
        }
    }

    /**
     * Loads all armies from the armies resource folder.
     *
     * @return a list of armies.
     */
    public static List<Army> loadAllArmies() {
        ArrayList<Army> armies = new ArrayList<>();

        URI path;
        try {
            path = ArmyFileHandler.class.getResource("/armies").toURI();
        } catch (URISyntaxException e) {
            GUI.showAlert(Alert.AlertType.ERROR, "Could not load all armies...\n\n" + e.getStackTrace());
            e.printStackTrace();
            return null;
        }

        // TODO: Check for null values
        File armiesFolder = new File(path);
        for (File armyFile : armiesFolder.listFiles()) {
            try {
                Army loadedArmy = ArmyFileHandler.loadArmyFromFile(armyFile);
                armies.add(loadedArmy);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Failed to load army file (but ignoring it and moving on): " + armyFile);
            }
        }
        return armies;
    }

    /**
     * Writes the information about the current army to a file.
     *
     * @param filePath  The path of the file to write to. This should include the file extension as well.
     * @throws IOException if a writing operation fails.
     */
    public static void saveToFile(Army army, String filePath) throws IOException {
        File filePathObject = new File(filePath);

        // Create the directory to the file
        String[] directories = filePath.split("/");
        String[] dirs = Arrays.copyOf(directories, directories.length - 1); // Remove file name

        File directoryPath = new File(String.join("/", dirs));

        // If the directory does not already exist, and it fails in creating a new directory...
        if (!directoryPath.exists() && !directoryPath.mkdirs()) {
            throw new IOException(
                    "The directory to the file could not be created. Have you closed all other programs accessing the folder?" +
                            "\nAttempted to create this directory: " + directoryPath);
        }

        // Create the file
        // If an IOException happens here; let it propagate upwards.
        if (!filePathObject.createNewFile()) {
            // The file already exists
            throw new FileAlreadyExistsException("The file provided already exists.");
        }


        FileWriter writer = new FileWriter(filePath);
        writer.write(army.getName() + "\n");

        List<Unit> allUnits = army.getAllUnits();

        for (Unit unit : allUnits) {
            writer.write(String.format("%s,%s,%s\n", unit.getClass().getSimpleName(), unit.getName(), unit.getHealth()));
        }
        writer.close();
    }

    /**
     * Attempts to load an Army from the given file. In most cases you should just use
     * {@link ArmyFileHandler#loadArmy} as that method is simpler to use and requires
     * fewer lines of code. Use this method if you are loading a file from a different
     * directory than the default {@link ArmyFileHandler#armyPath army path}.
     *
     * @param armyFile
     *
     * @return
     * @throws IOException If the file is empty, or if malformed data is present.
     * @throws FileNotFoundException if the given file is not found.
     */
    public static Army loadArmyFromFile(File armyFile) throws IOException {
        // I acknowledge that the task specified a .csv file ending, however I argue that no matter the file extension
        // the file *can* be readable. The file name ending is just a way for the OS (and humans) to tell which program
        // should run the file. Thus, a file might still be readable regardless of the file ending, so instead of
        // asserting that the file ending is .csv, I attempt to read the file and throw an error if it can not be read.
        Scanner reader = new Scanner(armyFile);

        if (!(reader.hasNextLine())) throw new IOException("The file provided is empty.");

        // Assuming that the first line is the army name
        String armyName = reader.nextLine().strip();

        Army newArmy = new Army(armyName);

        // This will keep track of which line we're reading so that we can provide a
        // more useful error if the program stops at a malformed line in the file.
        int currentLine = 1;
        while (reader.hasNextLine()) {
            String data = reader.nextLine();
            currentLine++;

            // I refuse to avoid using the break statement in this case, although we've been taught to do so.
            // It is very clear what loop we're breaking out of, it follows the "logical" program flow, and it is
            // more readable, so I see no reason to introduce a new variable and an extra 2-3 lines of code.
            // I back up my point with this popular stack overflow question: https://stackoverflow.com/a/18188174
            // Or this similar question: https://stackoverflow.com/q/3922599

            // If the line is just a newline, we can assume we've come to the end of the file
            // This could alternatively be replaced with a continue statement since if the last line is simply
            // a newline, the while loop would anyway exit after this run. I like to be strict however, so I'm
            // assuming that a newline simply means "We're done".
            if (data.equals("\n")) break;

            // Now we can assume there is data meant to be read
            String[] splitData = data.split(",");

            // If the length is wrong - assume the data is corrupt and abort loading from file.
            if (!(splitData.length == 3)) {
                reader.close();
                throw new IOException(String.format("Malformed data on line %d when reading %s. Incorrect amount of values separated by comma.", currentLine, armyFile));
            }

            // Read the unit from file
            Unit unitToAdd;
            try {
                unitToAdd = Unit.createUnitFromString(splitData[0], splitData[1], Integer.parseInt(splitData[2]));
            } catch (NumberFormatException e) {
                reader.close();
                throw new NumberFormatException(String.format("Could not convert data to an Integer on line %d", currentLine));
            } catch (IllegalArgumentException e) {
                reader.close();
                throw new IllegalArgumentException(String.format("Could not create a unit from the data on line %d.", currentLine));
            } catch (Exception e) {
                reader.close();
                throw new IOException(String.format("An unknown error occurred when reading %s on line %d", armyFile, currentLine));
            }

            newArmy.add(unitToAdd);
        }

        reader.close();

        return newArmy;
    }
}

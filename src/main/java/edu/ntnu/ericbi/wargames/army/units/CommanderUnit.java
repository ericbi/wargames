package edu.ntnu.ericbi.wargames.army.units;

/** A Commander army unit. */
public class CommanderUnit extends CavalryUnit {
    /** Default attack power */
    final static protected int DEFAULT_ATTACK_POWER = 25;

    /** Default armor */
    final static protected int DEFAULT_ARMOR = 15;

    /**
     * Base constructor for a Commander unit.
     *
     * @param name  the name of the Commander unit
     * @param health  the health of the Commander unit
     */
    public CommanderUnit(String name, int health) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
    }
}

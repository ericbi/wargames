package edu.ntnu.ericbi.wargames.army.abc;

import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import edu.ntnu.ericbi.wargames.army.enums.UnitType;
import edu.ntnu.ericbi.wargames.exceptions.UnitNotImplementedException;
import edu.ntnu.ericbi.wargames.army.units.InfantryUnit;

import java.util.Objects;

import static edu.ntnu.ericbi.wargames.army.factories.UnitFactory.createUnit;

/**
 * An abstract base class used for army units. This class provides methods such as
 * attacking other units and storing health, attack, armor etc. stats of a unit.
 */
public abstract class Unit {
    /** Name of the unit. */
    private String name;

    /** The unit's attack power. */
    private int attack;

    /** The unit's armor. */
    private int armor;

    /** The unit's health. */
    private int health;


    /**
     * Unit constructor.
     *
     * @param name  the name of the unit
     * @param attack  the attack power of the unit
     * @param armor  the unit's armor
     * @param health  the unit's health
     * @throws IllegalArgumentException if health parameter is less than 0
     */
    public Unit(String name, int attack, int armor, int health) {
        // Check if health value is valid
        if (health < 0) {
            throw new IllegalArgumentException("Health can not be less than 0.");
        }
        // I have intentionally initialized these values without checking
        // their values (apart from health). Checks for valid values is up to
        // each implementation of Unit to implement. (Rationale: What if for some
        // reason a test unit needs to be unbeatable? What if a unit is supposed to
        // never be able to deal damage and thus needs negative <big number> damage?
        // And a unit doesn't necessarily need a name, right?)
        this.name = name;
        this.attack = attack;
        this.armor = armor;
        this.health = health;
    }


    /**
     * Get the attack bonus for the unit.
     *
     * @return the attack bonus for the unit, depending on the state of
     *         the battle and/or unit when the method is called
     */
    public abstract int getAttackBonus();

    // TODO: Make terrain attack bonus a part of the getAttackBonus
    /**
     * Get the attack bonus for the unit in the given terrain
     *
     * @return the attack bonus for the unit, depending on the state of
     *         the battle and/or unit when the method is called
     */
    public abstract int getTerrainAttackBonus(TerrainType terrain);


    /**
     * Get the resist bonus for the unit.
     *
     * @return the resist bonus for the unit, depending on the state of
     *         the battle and/or unit when the method is called
     */
    public abstract int getResistBonus();

    // TODO: Make terrain defense bonus a part of the getResistBonus method
    /**
     * Get the defense bonus for the unit in the given terrain
     *
     * @return the attack bonus for the unit, depending on the state of
     *         the battle and/or unit when the method is called
     */
    public abstract int getTerrainDefenseBonus(TerrainType terrain);

    /**
     * Get a deep copy of the unit.
     *
     * @return a deep copy of the unit.
     */
    public abstract Unit copy();

    /**
     * Get the value of name.
     *
     * @return the {@link Unit#name} of the {@link Unit}
     */
    public final String getName() {
        return name;
    }


    /**
     * Get the value of attack.
     *
     * @return the {@link Unit#attack} power of the {@link Unit}
     */
    public final int getAttack() {
        return attack;
    }


    /**
     * Get the value of armor.
     *
     * @return the {@link Unit#armor} of the {@link Unit}
     */
    public final int getArmor() {
        return armor;
    }


    /**
     * Get the health of the unit.
     *
     * @return the {@link Unit#health} of the {@link Unit}
     */
    public final int getHealth() {
        return health;
    }


    /**
     * Set the health to the given value. If you want the unit to
     * take damage, use {@link Unit#takeDamage} instead.
     *
     * @param health  the new health value
     * @throws IllegalArgumentException when target health is lower than 0
     */
    public void setHealth(int health) {
        if (health < 0) {
            throw new IllegalArgumentException("Health value can not be set to less than 0.");
        }
        this.health = health;
    }


    /**
     * Handles the event of the Unit taking damage.
     *
     * @param damage  the damage taken. {@link Unit#health Health} is reduced by this amount
     * @throws IllegalArgumentException when the resulting health would be negative.
     */
    // I have chosen to implement this method so that units extending this class can override what happens
    // when it takes damage if necessary (like the Infantry unit that will need to track amount of times it
    // has been attacked).
    public void takeDamage(int damage) {
        if (this.getHealth() - damage < 0) {
            throw new IllegalArgumentException("The amount of damage taken would result in negative health.");
        }
        this.setHealth(this.getHealth() - damage);
    }


    /**
     * Attacks an opponent (Unit). The {@link Unit opponent} takes damage following this formula:
     * (attackDamage + attackBonus) - (opponentArmor + opponentResistBonus)
     *
     * @param opponent the opponent {@link Unit} to attack
     */
    public void attack(Unit opponent, TerrainType terrain) {
        // Calculate the damage done
        int damage = (this.getAttack() + this.getAttackBonus() + this.getTerrainAttackBonus(terrain)) -
                     (opponent.getArmor() + opponent.getResistBonus() + opponent.getTerrainDefenseBonus(terrain));

        // Here I'm assuming the opponent should not heal if the armor
        // and resist is higher than the attack damage and attack bonus
        // Damage should *never* be negative.
        if (damage < 0) {
            damage = 0;
        }

        if (opponent.getHealth() - damage < 0) {
            opponent.setHealth(0);
        }
        else {
            opponent.takeDamage(damage);
        }
    }

    /**
     * Creates a unit of the given unit type class name.
     *
     * @param unitClassName  The class name of the unit. This could for example be "InfantryUnit" which would create a new {@link InfantryUnit}.
     * @param name  The name of the new unit.
     * @param health  The health of the new unit.
     * @return a new instance of the given unit.
     * @throws IllegalArgumentException if the given unit is not valid.
     */
    public static Unit createUnitFromString(String unitClassName, String name, int health) throws UnitNotImplementedException {
        try {
            return createUnit(UnitType.valueOf(unitClassName), name, health);
        } catch (IllegalArgumentException e) {
            throw new UnitNotImplementedException("The unit '" + unitClassName + "' was not found in the UnitTyp enum. Could not create the unit.");
        }
    }


    /**
     * Returns the string representation of a unit.
     *
     * @return {@literal <UnitName> (<UnitType>) has <Health> health, <Attack> (+<AttackBonus>)
     *                   attack and <Armor> (+<ResistBonus>) armor.}
     */
    @Override
    public String toString() {
        return String.format(
                "%s (%s) has %s health, %s (+%s) attack and %s (+%s) armor.",
                this.getName(), this.getClass().getSimpleName(), this.getHealth(),
                this.getAttack(), this.getAttackBonus(), this.getArmor(), this.getResistBonus());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Unit)) return false;
        Unit unit = (Unit) o;
        return getAttackBonus() == ((Unit) o).getAttackBonus() && getResistBonus() == ((Unit) o).getResistBonus() &&
                (attack == unit.attack && armor == unit.armor && health == unit.health && Objects.equals(name, unit.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, attack, armor, health);
    }
}

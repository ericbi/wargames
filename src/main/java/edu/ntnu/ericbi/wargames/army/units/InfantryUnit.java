package edu.ntnu.ericbi.wargames.army.units;

import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;


/** An Infantry army melee unit. */
public class InfantryUnit extends Unit {
    /** Default attack value for an Infantry */
    final static protected int DEFAULT_ATTACK_POWER = 15;

    /** Default armor value for an Infantry */
    final static protected int DEFAULT_ARMOR = 10;

    /** An infantry unit's attack bonus */
    final static protected int DEFAULT_ATTACK_BONUS = 2;

    /** An infantry unit's melee resist bonus */
    final static protected int DEFAULT_RESIST_BONUS = 1;

    /**
     * Base constructor for an Infantry unit.
     *
     * @param name  the name of the infantry unit
     * @param health  the infantry unit's health
     */
    public InfantryUnit(String name, int health) {
        super(name, DEFAULT_ATTACK_POWER, DEFAULT_ARMOR, health);
    }

    @Override
    public int getAttackBonus() {
        return DEFAULT_ATTACK_BONUS;
    }

    @Override
    public int getTerrainAttackBonus(TerrainType terrain) {
        if (terrain.equals(TerrainType.HILL)) return 2;
        return 0;
    }

    @Override
    public int getResistBonus() {
        return DEFAULT_RESIST_BONUS;
    }

    @Override
    public int getTerrainDefenseBonus(TerrainType terrain) {
        if (terrain.equals(TerrainType.FOREST)) return 1;
        return 0;
    }

    @Override
    public Unit copy() {
        return new InfantryUnit(getName(), getHealth());
    }
}

package edu.ntnu.ericbi.wargames;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * The main class for interfacing the javaFX GUI. This is the entry point for the javaFX
 * application, and contains static helper methods for simple loading of FXML pages and
 * showing alerts.
 */
public class GUI extends Application {
    private static Stage primaryStage;
    public static final String pagesDirectory = "/gui/pages/";

    public static final double MIN_WIDTH = 800;
    public static final double MIN_HEIGHT = 600;

    @Override
    public void start(Stage parsedPrimaryStage) {
        primaryStage = parsedPrimaryStage;

        // Set window logo and title
        String logoUrl = String.valueOf(getClass().getResource("/gui/media/WarGames - logo.png"));
        Image logo = new Image(logoUrl);
        primaryStage.getIcons().add(logo);
        primaryStage.setTitle("WarGames");

        try {
            setStageScene(primaryStage, "/gui/pages/mainPage.fxml");
        } catch (IOException e) {
            e.printStackTrace();
            //noinspection UnnecessaryReturnStatement
            return;
        }
    }

    // #######################################################
    // ################## HELPER FUNCTIONS ###################
    // #######################################################

    /**
     * A higher level method to set the primary stage scene to an FXML file.
     *
     * @param page  The fxml file name relative to the {@link GUI#pagesDirectory default pages directory}.
     */
    public static void setPage(String page) {
        try {
            setPrimaryStageScene(pagesDirectory + page);
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new NullPointerException("Could not set new page. Check that the FXML file is loaded correctly!");
    }


    /**
     * Sets the primary stage to a new scene loaded with the FXML from the given file path.
     *
     * @param fxmlFilePath  The directory of the FXML file to load, relative to the resources folder.
     * @throws IOException if an errors happens when loading the FXML file.
     */
    public static void setPrimaryStageScene(String fxmlFilePath) throws IOException {
        setStageScene(primaryStage, fxmlFilePath);
    }

    /**
     * Sets the given stage's scene to the given FXML page.
     *
     * @param stage  The stage to set to. The scene of this stage will be loaded with the FXML file.
     * @param fxmlFilePath  The directory of the FXML file to load, relative to the resources folder.
     * @throws FileNotFoundException if the loader can not find the fxml file at the provided path.
     * @throws IOException if an error happens while loading the FXML file with the FXMLLoader.
     */
    public static void setStageScene(Stage stage, String fxmlFilePath) throws IOException {
        // Get previous window dimensions so that the window does
        // not change size when the scene is switched.
        double previousWidth = stage.getWidth();
        double previousHeight = stage.getHeight();

        // Try to get the FXML file path URL
        URL UrlToFxmlFile;
        try {
            UrlToFxmlFile = GUI.class.getResource(fxmlFilePath);
            if (UrlToFxmlFile == null) throw new FileNotFoundException("Could not get the resource. The FXML URL returned null.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Something went wrong when getting the FXML URL.");
            throw new FileNotFoundException(e.getMessage());
        }

        // Load the File
        Parent root = FXMLLoader.load(UrlToFxmlFile);
        if (root == null)
            throw new IOException("Something wrong happened when loading the FXML file: " + fxmlFilePath);

        // Ensure all stages do not scale smaller than this global minimum width and height
        stage.setMinWidth(MIN_WIDTH);
        stage.setMinHeight(MIN_HEIGHT);

        // Set the window size to the previous windows size
        stage.setWidth(previousWidth);
        stage.setHeight(previousHeight);

        // Set the scene and show it
        stage.setScene(new Scene(root));
        stage.show();
    }

    /**
     * Shows an alert box of the given type. The new window created is
     * blocking, and the GUI will resume once the alert is closed.
     *
     * @param alertType  The type of alert to display.
     * @param message  The message to display.
     */
    public static void showAlert(Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType);
        alert.setHeaderText(message);
        alert.showAndWait();
    }
}

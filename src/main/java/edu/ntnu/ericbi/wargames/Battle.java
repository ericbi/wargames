package edu.ntnu.ericbi.wargames;

import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import edu.ntnu.ericbi.wargames.exceptions.EmptyArmyException;


/** A class representing a battle between two armies */
public class Battle {
    /** The first army participating in the battle */
    public final Army armyOne;

    /** The second army participating in the battle */
    public final Army armyTwo;

    private final TerrainType terrain;


    /** Constructor to initialize a battle with two armies */
    public Battle(Army armyOne, Army armyTwo, TerrainType chosenTerrain) throws EmptyArmyException {
        if (armyOne == null || armyTwo == null)
            throw new EmptyArmyException("One of the armies in the battle is null.");

        // Create copy of armies so that the original instances are not affected:
        this.armyOne = new Army(armyOne.getName(), armyOne.getAllUnits());
        this.armyTwo = new Army(armyTwo.getName(), armyTwo.getAllUnits());
        terrain = chosenTerrain;
    }


    /**
     * Simulates a battle between two armies. This is done by first taking a random unit from the
     * first army which attacks a random unit from the second army. Then the roles are reversed.
     * This is repeated until one of the armies do not have units left.
     *
     * @return the winning army.
     * @throws IllegalStateException if both armies start empty.
     */
    public Army simulate() throws IllegalStateException {
        // Ensure that all dead units are filtered out from start so that we can
        // assume that all troops in the armies are alive to begin with
        armyOne.getAllUnits().forEach(unit -> {
            if (unit.getHealth() <= 0) armyOne.remove(unit);
        });

        armyTwo.getAllUnits().forEach(unit -> {
            if (unit.getHealth() <= 0) armyTwo.remove(unit);
        });

        // Run simulation steps until finished
        //noinspection StatementWithEmptyBody
        while (simulateStep() == null);
        return armyOne.hasUnits() ? armyOne : armyTwo;
    }


    /**
     * Simulates a single attack from both armies. All units that die in combat are also removed from
     * the army. Running this method again and again will eventually return the winning Army.
     *
     * @return the winning army if one army eliminates the other. Returns null if both armies still exist after the attack round.
     * @throws IllegalStateException if both armies start empty.
     */
    public Army simulateStep() throws IllegalStateException {
        // Ensure that armies are not empty
        if (!armyOne.hasUnits() && !armyTwo.hasUnits())
            throw new IllegalStateException("Both armies can not be empty or filled with only dead (0 health) units when running simulate().");

        // Army 1 attacks army 2
        Unit attacker = armyOne.getRandom();
        Unit defender = armyTwo.getRandom();
        attacker.attack(defender, terrain);
        if (defender.getHealth() <= 0) armyTwo.remove(defender);

        // Check if armyTwo is now completely killed
        if (!armyTwo.hasUnits()) return armyOne;

        // Now army 2 attacks army 1
        defender = armyOne.getRandom();
        attacker  = armyTwo.getRandom();
        attacker.attack(defender, terrain);
        if (defender.getHealth() <= 0) armyOne.remove(defender);

        // Check if armyOne is now completely killed
        if (!armyOne.hasUnits()) return armyTwo;

        // Return null if none of the armies has won yet.
        return null;
    }

    @Override
    public String toString() {
        return String.format("A battle between army \"%s\" and army \"%s\"", this.armyOne.getName(), this.armyTwo.getName());
    }
}

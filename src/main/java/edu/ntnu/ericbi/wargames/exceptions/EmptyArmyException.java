package edu.ntnu.ericbi.wargames.exceptions;

/**
 * Thrown when an army is empty and is not supposed to be empty.
 */
public class EmptyArmyException extends Exception {
    public EmptyArmyException(String errorMessage) {
        super(errorMessage);
    }
}

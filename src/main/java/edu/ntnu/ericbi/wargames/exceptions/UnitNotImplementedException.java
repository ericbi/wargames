package edu.ntnu.ericbi.wargames.exceptions;

/**
 * Exception thrown when a unit is not implemented. This can be used with factories
 * when the method is called with a unit argument that is not yet implemented and added
 * in the UnitType enum.
 */
public class UnitNotImplementedException extends Exception {
    public UnitNotImplementedException(String errorMessage) {
        super(errorMessage);
    }
}

package edu.ntnu.ericbi.wargames.gui.controllers;

import edu.ntnu.ericbi.wargames.Battle;
import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.GUI;
import edu.ntnu.ericbi.wargames.army.ArmyFileHandler;
import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import edu.ntnu.ericbi.wargames.gui.components.ArmyListElementComponent;
import edu.ntnu.ericbi.wargames.gui.components.TerrainTypeListElementComponent;
import edu.ntnu.ericbi.wargames.gui.decorators.ListElementDecorator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BattleEditorController {
    private List<ArmyListElementComponent> armySelection1;
    private List<ArmyListElementComponent> armySelection2;
    private List<TerrainTypeListElementComponent> terrainSelection;

    @FXML
    private VBox army1VBox;

    @FXML
    private VBox army2VBox;

    @FXML
    private VBox terrainVBox;

    @FXML
    private void homeBtnClicked(ActionEvent event) {
        GUI.setPage("mainPage.fxml");
    }

    @FXML
    private void battleBtnClicked(ActionEvent event) {
        // Get the first selected army
        List<ArmyListElementComponent> selectedArmy1 = armySelection1.stream().filter(c -> c.selected == true).collect(Collectors.toList());
        Army army1;
        try {
            army1 = selectedArmy1.get(0).getArmy();
        } catch (Exception e) {
            GUI.showAlert(Alert.AlertType.ERROR, "Could not get selected army 1. Have you chosen an army?");
            return;
        }

        // Get the second selected army
        List<ArmyListElementComponent> selectedArmy2 = armySelection2.stream().filter(c -> c.selected == true).collect(Collectors.toList());
        Army army2;
        try {
            army2 = selectedArmy2.get(0).getArmy();
        } catch (Exception e) {
            GUI.showAlert(Alert.AlertType.ERROR, "Could not get selected army 2. Have you chosen a second army?");
            return;
        }

        // Get the selected terrain
        List<TerrainTypeListElementComponent> selectedTerrain = terrainSelection.stream().filter(c -> c.selected == true).collect(Collectors.toList());
        TerrainType terrain;
        try {
            terrain = selectedTerrain.get(0).terrain;
        } catch (Exception e) {
            GUI.showAlert(Alert.AlertType.ERROR, "Could not get selected terrain. Have you chosen a terrain?");
            return;
        }

        // Create battle object
        Battle battle;
        try {
            battle = new Battle(army1, army2, terrain);
        } catch (Exception e) {
            GUI.showAlert(Alert.AlertType.ERROR, "Could not create a battle object...");
            return;
        }

        // Redirect to Battle page
        BattleSimulationController.battle = battle;
        Stage battleStage = new Stage();
        battleStage.setOnCloseRequest(e -> {
            if (!(BattleSimulationController.getBattleThread() == null)) {
                BattleSimulationController.getBattleThread().killThread();
            }

            if (!(BattleSimulationController.getAnimationThread() == null)) {
                BattleSimulationController.getAnimationThread().killThread();
            }
            battleStage.close();
        });

        battleStage.setTitle("Battle!");
        battleStage.initOwner(army1VBox.getScene().getWindow());
        battleStage.initModality(Modality.APPLICATION_MODAL);
        try {
            GUI.setStageScene(battleStage, "/gui/pages/battleSimulationPage.fxml");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        return;
    }

    @FXML
    private void resetBtnClicked(ActionEvent event) {
        armySelection1.forEach(ListElementDecorator::styleListElement);
        armySelection1.forEach(comp -> comp.selected = false);
        armySelection2.forEach(ListElementDecorator::styleListElement);
        armySelection2.forEach(comp -> comp.selected = false);
        terrainSelection.forEach(comp -> comp.setOpacity(0.2));
        terrainSelection.forEach(comp -> comp.selected = false);
    }

    @FXML
    private void initialize() {
        // Create two deep copies to avoid unexpected behaviour later
        List<Army> armies1 = ArmyFileHandler.loadAllArmies();
        List<Army> armies2 = ArmyFileHandler.loadAllArmies();

        this.armySelection1 = armiesToComponents(armies1);
        this.armySelection2 = armiesToComponents(armies2);
        this.terrainSelection = terrainsToComponents();

        // Fill the armies in the GUI.
        fillArmies(army1VBox, this.armySelection1);
        fillArmies(army2VBox, this.armySelection2);
        fillTerrains(this.terrainSelection);

        // Add event listeners on all components
        this.armySelection1.forEach(component -> {
            component.setOnMouseClicked(event -> {
                // When clicked, deselect all components
                armySelection1.forEach(ListElementDecorator::styleListElement);
                armySelection1.forEach(comp -> comp.selected = false);

                // Then select the one that was clicked
                ListElementDecorator.styleListElementAsSelected(component);
                component.selected = true;
            });
        });

        this.armySelection2.forEach(component -> {
            component.setOnMouseClicked(event -> {
                // When clicked, deselect all components
                armySelection2.forEach(ListElementDecorator::styleListElement);
                armySelection2.forEach(comp -> comp.selected = false);

                // Then select the one that was clicked
                ListElementDecorator.styleListElementAsSelected(component);
                component.selected = true;
            });
        });

        this.terrainSelection.forEach(component -> {
            component.setOnMouseClicked(event -> {
                // When clicked, deselect all components
                terrainSelection.forEach(comp -> comp.setOpacity(0.2));
                terrainSelection.forEach(comp -> comp.selected = false);

                // Then select the one that was clicked
                component.setOpacity(1);
                component.selected = true;
            });
        });
    }

    private List<ArmyListElementComponent> armiesToComponents(List<Army> armies) {
        return armies.stream().map(ArmyListElementComponent::new).collect(Collectors.toList());
    };

    private void fillArmies(VBox parent, List<ArmyListElementComponent> armyComponents) {
        parent.getChildren().addAll(armyComponents);
    }

    private List<TerrainTypeListElementComponent> terrainsToComponents() {
        return List.of(TerrainType.values()).stream()
                                            .map(TerrainTypeListElementComponent::new)
                                            .collect(Collectors.toList());
    }

    private void fillTerrains(List<TerrainTypeListElementComponent> terrains) {
        terrainVBox.getChildren().addAll(terrains);
    }
}

package edu.ntnu.ericbi.wargames.gui.components;

import edu.ntnu.ericbi.wargames.GUI;
import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.gui.controllers.BattleEditorController;
import edu.ntnu.ericbi.wargames.gui.decorators.ListElementDecorator;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URISyntaxException;

/**
 * A GUI component to show an Army in a list. This is an extension of a VBox with
 * methods to update component information and create the component content.
 */
public class ArmyListElementComponent extends VBox {
    public boolean selected = false;
    private Army army;

    private Text titleName;
    private Text hpText;
    private Text attackText;
    private Text shieldText;

    private EventHandler onClickMethod;

    /**
     * Create and fill the component content from a given army.
     *
     * @param army  The army to create the component from.
     */
    public ArmyListElementComponent(Army army) {
        super();
        this.army = army;
        initializeComponent();
    }

    /**
     * Updates the information in the component. This information is updated
     * using the information from the "connected"/corresponding army.
     */
    public void updateInformation() {
        titleName.setText(army.getName());
        hpText.setText(army.getTotalHealth() + " HP");
        attackText.setText(String.format("%.2f", army.getAverageAttack()));
        shieldText.setText(String.format("%.2f", army.getAverageArmor()));
    }

    /**
     * Get the associated army to this army list element component.
     *
     * @return the associated army.
     */
    public Army getArmy() {
        return army;
    }

    private void initializeComponent() {
        // Create elements
        titleName = new Text();
        HBox statisticsBox = createStatisticsBox();

        // Add elements for correct node structure
        this.getChildren().add(titleName);
        this.getChildren().add(statisticsBox);

        // Style elements
        ListElementDecorator.styleListElement(this);
        ListElementDecorator.styleTitleName(titleName);

        // Fill in values
        updateInformation();
        return;
    }

    private HBox createStatisticsBox() {
        // Create elements
        HBox statisticsBox = new HBox();
        HBox otherStatsBox = createOtherStats();
        HBox hpStatBox = new HBox();
        hpText = new Text();

        // Add elements for correct node structure
        hpStatBox.getChildren().add(hpText);
        statisticsBox.getChildren().add(hpStatBox);
        statisticsBox.getChildren().add(otherStatsBox);

        // Style elements
        ListElementDecorator.styleStatisticsBox(statisticsBox);
        ListElementDecorator.styleOtherStatsBox(otherStatsBox);
        ListElementDecorator.styleHpStatBox(hpStatBox);

        return statisticsBox;
    }

    private HBox createOtherStats() {
        HBox otherStatsBox = new HBox();

        // Create elements
        attackText = new Text();
        ImageView attackImage = new ImageView();
        shieldText = new Text();
        ImageView shieldImage = new ImageView();

        // Load images
        String imgURI = "";
        try {
            imgURI = String.valueOf(BattleEditorController.class.getResource("/gui/media/SwordIcon.png").toURI());
            attackImage.setImage(new Image(imgURI));
            imgURI = String.valueOf(BattleEditorController.class.getResource("/gui/media/ArmorIcon.png").toURI());
            shieldImage.setImage(new Image(imgURI));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            GUI.showAlert(Alert.AlertType.ERROR, "Could not find image file: " + imgURI);
        }

        // Add elements for correct node structure
        otherStatsBox.getChildren().add(attackText);
        otherStatsBox.getChildren().add(attackImage);
        otherStatsBox.getChildren().add(shieldText);
        otherStatsBox.getChildren().add(shieldImage);

        // Style elements
        ListElementDecorator.styleStatisticsBarIconImage(attackImage);
        ListElementDecorator.styleStatisticsBarIconImage(shieldImage);
        ListElementDecorator.styleStatisticsBarText(attackText);
        ListElementDecorator.styleStatisticsBarText(shieldText);

        return otherStatsBox;
    }
}

package edu.ntnu.ericbi.wargames.gui.decorators;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Contains static methods for styling nodes related to list element components.
 */
public class ListElementDecorator {
    /**
     * Style the list element node. This methods styles only the list element
     * and not its children. To style other parts of the list element, other methods
     * found in this class must be used on the children.
     *
     * @param vbox  The vbox to style as a ListElement.
     */
    public static void styleListElement(VBox vbox) {
        vbox.setStyle("-fx-border-color: black; -fx-background-color: white; -fx-padding: 10px 0 0 0");
        vbox.setPrefHeight(60);
        vbox.setAlignment(Pos.CENTER);
        vbox.setCursor(Cursor.HAND);
    }

    /**
     * Style the title of a list element.
     *
     * @param nameText  The text to style as the title of the list element.
     */
    public static void styleTitleName(Text nameText) {
        nameText.setFont(Font.font("Calibri", FontWeight.NORMAL, FontPosture.REGULAR, 22));
    }

    /**
     * Style the statistics box of a list element. A statistics box is an hbox
     * that defines the area within which statistics should be displayed.
     * This method also sets layout information (as in grow attributes, margins etc).
     *
     * @param statBox  The statistics box to style.
     */
    public static void styleStatisticsBox(HBox statBox) {
        VBox.setVgrow(statBox, Priority.ALWAYS);
    }

    /**
     * Styles the HP statistics box. The HP statistics box is an hbox that
     * defines the area within which health-related statistics should be displayed.
     * This method also sets layout information (as in grow attributes, margins etc).
     *
     * @param hpBox  The hbox to style as an HP box.
     */
    public static void styleHpStatBox(HBox hpBox) {
        HBox.setHgrow(hpBox, Priority.ALWAYS);
        hpBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setMargin(hpBox, new Insets(0, 0, 0, 15));
    }

    /**
     * Style the "other stats" box. The other stats box is an hbox that defines the area
     * within which other information such as attack power and resist power is displayed.
     * This method also sets layout information (as in grow attributes, margins etc).
     *
     * @param otherStatsBox  The hbox to style as an "other stats" box.
     */
    public static void styleOtherStatsBox(HBox otherStatsBox) {
        HBox.setHgrow(otherStatsBox, Priority.ALWAYS);
        otherStatsBox.setAlignment(Pos.CENTER_RIGHT);
    }

    /**
     * Sets styling for an icon-like image used in a status/statisics bar.
     * This can be used on the shield and sword PNGs for example and works well in
     * status/statistics bars since it also adds a 15px margin to the right.
     *
     * @param image  The image to style.
     */
    public static void styleStatisticsBarIconImage(ImageView image) {
        HBox.setMargin(image, new Insets(0, 15, 0, 0));
        image.setFitWidth(15);
        image.setFitHeight(15);
    }

    /**
     * Style text that is meant to be in the "statistics bar". Examples of text nodes
     * like this can be a Text node with the text value "15 HP" next to a health icon.
     *
     * @param textNode  The text node to style.
     */
    public static void styleStatisticsBarText(Text textNode) {
        HBox.setMargin(textNode, new Insets(0, 5, 0, 0));
    }

    /**
     * Styles the vbox as a selected list element.
     * @param vbox
     */
    public static void styleListElementAsSelected(VBox vbox) {
        vbox.setStyle("-fx-border-color: black; -fx-background-color: darkgray");
    };
}

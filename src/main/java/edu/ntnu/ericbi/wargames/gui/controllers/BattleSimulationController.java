package edu.ntnu.ericbi.wargames.gui.controllers;

import edu.ntnu.ericbi.wargames.Battle;
import edu.ntnu.ericbi.wargames.GUI;
import edu.ntnu.ericbi.wargames.army.Army;
import edu.ntnu.ericbi.wargames.army.abc.Unit;
import edu.ntnu.ericbi.wargames.gui.components.ArmyListElementComponent;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Battle simulation page controller. Contains functionality for running the battle simulation.
 */
public class BattleSimulationController {
    /** The battle that is being simulated */
    public static Battle battle;

    @FXML
    private VBox army1box;

    @FXML
    private VBox army2box;

    @FXML
    private Button startStopBtn;

    private static BattleThread battleThread;
    private static AnimationThread animationThread;

    /**
     * Returns the thread that runs battle steps. This value can be null if the thread has not been started yet.
     *
     * @return the battle thread or null.
     */
    public static BattleThread getBattleThread() {
        return battleThread;
    }

    /**
     * Returns the thread that updates GUI elements. This value can be null if the thread has not been started yet.
     *
     * @return the animation thread or null.
     */
    public static AnimationThread getAnimationThread() {
        return animationThread;
    }

    @FXML
    private void startStopBtnClicked() {
        if (startStopBtn.getText().equals("Start")) {
            startStopBtn.setText("Stop");
            battleThread = new BattleThread(250);
            animationThread = new AnimationThread(2);
            battleThread.start();
            animationThread.start();
        } else {
            startStopBtn.setText("Start");
            if (!(battleThread == null)) {
                battleThread.killThread();
                battleThread = null;
            }
            if (!(animationThread == null)) {
                animationThread.killThread();
                animationThread = null;
            }
        }
    }

    private void updateBattleView() {
        // Get all the different units for army1
        List<Unit> armyOneCommanderUnits = battle.armyOne.getCommanderUnits();
        List<Unit> armyOneInfantryUnits = battle.armyOne.getInfantryUnits();
        List<Unit> armyOneCavalryUnits = battle.armyOne.getCavalryUnits();
        List<Unit> armyOneRangedUnits = battle.armyOne.getRangedUnits();

        // Get all the different units for army2
        List<Unit> armyTwoCommanderUnits = battle.armyTwo.getCommanderUnits();
        List<Unit> armyTwoInfantryUnits = battle.armyTwo.getInfantryUnits();
        List<Unit> armyTwoCavalryUnits = battle.armyTwo.getCavalryUnits();
        List<Unit> armyTwoRangedUnits = battle.armyTwo.getRangedUnits();

        // An army is just a set of units, so I can reuse the ArmyListElementComponent builder
        // to create list elements for each "set" of units (e.g. Infantry units, Cavalry units etc.)
        ArmyListElementComponent armyOneCommandersListElement = new ArmyListElementComponent(new Army(battle.armyOne.getName() + " - Commanders", armyOneCommanderUnits));
        ArmyListElementComponent armyOneInfantriesListElement = new ArmyListElementComponent(new Army(battle.armyOne.getName() + " - Infantries", armyOneInfantryUnits));
        ArmyListElementComponent armyOneCavalriesListElement = new ArmyListElementComponent(new Army(battle.armyOne.getName() + " - Cavalries", armyOneCavalryUnits));
        ArmyListElementComponent armyOneRangersListElement = new ArmyListElementComponent(new Army(battle.armyOne.getName() + " - Rangers", armyOneRangedUnits));

        ArmyListElementComponent armyTwoCommandersListElement = new ArmyListElementComponent(new Army(battle.armyTwo.getName() + " - Commanders", armyTwoCommanderUnits));
        ArmyListElementComponent armyTwoInfantriesListElement = new ArmyListElementComponent(new Army(battle.armyTwo.getName() + " - Infantries", armyTwoInfantryUnits));
        ArmyListElementComponent armyTwoCavalriesListElement = new ArmyListElementComponent(new Army(battle.armyTwo.getName() + " - Cavalries", armyTwoCavalryUnits));
        ArmyListElementComponent armyTwoRangersListElement = new ArmyListElementComponent(new Army(battle.armyTwo.getName() + " - Rangers", armyTwoRangedUnits));

        army1box.getChildren().clear();
        army2box.getChildren().clear();

        army1box.getChildren().addAll(armyOneCommandersListElement, armyOneInfantriesListElement, armyOneCavalriesListElement, armyOneRangersListElement);
        army2box.getChildren().addAll(armyTwoCommandersListElement, armyTwoInfantriesListElement, armyTwoCavalriesListElement, armyTwoRangersListElement);
    }

    @FXML
    private void initialize() {
        updateBattleView();
    }

    /**
     * A thread responsible for simulating battle steps / attacks.
     */
    public static class BattleThread extends Thread {
        private final Battle battle;
        private boolean isRunning = false;
        private final int attacksPerSecond;

        /**
         * Creates a new thread responsible for simulating battle steps / attacks.
         *
         * @param attacksPerSecond  How many attacks to simulate per second (approximate).
         */
        public BattleThread(int attacksPerSecond) {
            super();
            this.battle = BattleSimulationController.battle;
            this.attacksPerSecond = attacksPerSecond;
        }

        /**
         * Runs the battle thread.
         */
        public void run() {
            isRunning = true;

            Army winningArmy = battle.simulateStep();
            while (isRunning && winningArmy == null) {
                try {
                    Thread.sleep(1000/attacksPerSecond);
                } catch (InterruptedException e) {
                    return;
                }
                winningArmy = battle.simulateStep();
            }

            Army finalWinningArmy = winningArmy;
            Platform.runLater(() -> GUI.showAlert(Alert.AlertType.INFORMATION, "We have a winner! The winning army was: " + finalWinningArmy.toString()));
        }

        /**
         * Requests the thread to stop. The thread will finish execution by escaping the
         * main while loop. Note that the thread does not terminate immediately when this
         * method is called.
         */
        public void killThread() {
            isRunning = false;
            this.interrupt();
        }
    }

    /**
     * A thread responsible for updating GUI elements with new data.
     */
    public class AnimationThread extends Thread {
        private boolean isRunning = false;
        private final int updatesPerSecond;
        /**
         * A constructor to create a new thread.
         *
         * @param updatesPerSecond  The amount of times to update the animations.
         */
        public AnimationThread(int updatesPerSecond) {
            super();
            this.updatesPerSecond = updatesPerSecond;
        }

        /**
         * Runs the thread.
         */
        public void run() {
            isRunning = true;
            // Update the army cards
            while (isRunning) {
                Platform.runLater(BattleSimulationController.this::updateBattleView);
                // Delay execution
                try {
                    Thread.sleep(1000/updatesPerSecond);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }


        /**
         * Requests the thread to stop. The thread will finish execution by escaping the
         * main while loop. Note that the thread does not terminate immediately when this
         * method is called.
         */
        public void killThread() {
            isRunning = false;
            this.interrupt();
        }
    }
}

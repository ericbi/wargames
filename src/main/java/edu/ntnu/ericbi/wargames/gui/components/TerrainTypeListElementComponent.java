package edu.ntnu.ericbi.wargames.gui.components;

import edu.ntnu.ericbi.wargames.army.enums.TerrainType;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.net.URL;

/**
 * A list element component for terrain types. This is a component used to display
 * a selectable terrain type in a list-like vbox.
 */
public class TerrainTypeListElementComponent extends VBox {
    /** The {@TerrainType terrain type} associated with the component. */
    public final TerrainType terrain;
    /** Whether the terrain type is selected or not. */
    public boolean selected = false;

    /**
     * Creates the component.
     *
     * @param terrain  The terrain to list.
     */
    public TerrainTypeListElementComponent(TerrainType terrain) {
        super();
        this.terrain = terrain;

        // Style vbox
        this.setPrefHeight(100);
        this.setAlignment(Pos.CENTER);
        URL imgFile = getClass().getResource(terrain.imgUrl);

        String imgPath = null;
        if (!(imgFile == null)) {
            imgPath = imgFile.getPath();
        }

        this.setStyle(
                (imgPath == null)? "" : "-fx-background-image: url(file://" + imgPath + ");" +
                "-fx-border-color: black;"
        );
        this.setOpacity(0.2);
        this.setCursor(Cursor.HAND);

        // Create and style title
        Text title = new Text();
        title.setText(terrain.name());
        title.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 28));
        title.setFill(Color.WHITE);
        title.setStroke(Color.BLACK);
        title.setStrokeWidth(1.5);

        // Add title
        this.getChildren().add(0, title);
    }
}

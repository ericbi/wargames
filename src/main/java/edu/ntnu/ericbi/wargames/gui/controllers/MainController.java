package edu.ntnu.ericbi.wargames.gui.controllers;

import edu.ntnu.ericbi.wargames.GUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Main page controller.
 */
public class MainController {
    @FXML
    private void battleSimulationBtnClicked(ActionEvent event) {
        GUI.setPage("battleEditorPage.fxml");
    }
}

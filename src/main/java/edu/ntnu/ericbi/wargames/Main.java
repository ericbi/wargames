package edu.ntnu.ericbi.wargames;

import javafx.application.Application;


public class Main {
    public static void main(String[] args) {
        Application.launch(GUI.class, args);
    }
}

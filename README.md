# About the project / project report
### Introduction
For the spring semester of 2022, students at NTNU taking the IDATT2001
(Programmering 2) course were given a project to work with throughout the
semester. This project was divided into 3 parts; tagged in this repository
at the different stages. WarGames is a battle simulation program where two
armies are simulated to fight against each other. This project is my solution
to the given task.

### Requirements & usability functions
The graphical user interface allows users to select two armies (all armies
are loaded from the @resources/armies/ folder) and a terrain, then view an
animated simulation of the two armies' fighting. Once the fight ends, the
user is notified which army that won, and also how many troops that remain
in that army. The user can then choose to start a battle between two new 
armies.

### Design
When designing the classes for armies, battles and units, I focused on 
modularizing each class to ensure loose coupling. What I mean by modularizing
is that each class should in theory be a self-contained functioning class
that is not dependent on the state of any other class and does not change
the state of other objects outside the class. Therefore, it was important
to create deep copies of objects in many situations (for example when creating
a Battle object). I find this type of design very intuitive and predictable.
There were some places where I could not follow this design - particularly
when it came to GUI - since states are important for the rendering of certain
elements.

To create army units, I used a factory design pattern. I used this in a way
to create units in a more simple and dynamic way. With the factory design
pattern, the type of unit to instantiate can be decided during runtime since
only a String/Enum is parsed to the factory method.

During the development of the GUI, I used two design patterns in particular.
The first was decorators. The decorator design pattern is the idea of
creating static class methods to style elements with multiple styles. Instead
of styling each component / node individually, you can reuse the decorators
and ensure that all changes to styling are the same across the application.
This design pattern also proved to be helpful in knowing where to look when
wanting to change the styling of elements.

The second design pattern was an idea I "stole" from the React.js design
principles, called component based design. The idea is that GUI elements
can be grouped together to form larger "components". With these components,
you can build a page with more abstract / larger elements. During run time
you can also update specific components instead of updating the whole page.
This allows for quicker GUI updates and less processing time.

### Implementation
The project was made with a very simple, javafx archetype with the maven build 
system. The only dependency used was javafx (for a GUI) and junit (for unit
testing). The application is run by simply using the javafx plugin command
"run": `mvn javafx:run`. For the JavaFX GUI I used FXML files to design the
base layout of the page, then filled in dynamic elements with javafx code. 

In this project, I found it natural to use enums since both units and
terrain types could only be one of a few choices. By using an enum, I restrict
which values that can be passed when for example selecting terrain or
creating a new Unit using the unit factory. This also provides more
documentation to the programmer, so that they know which values are valid.

Throughout the process of developing this project, I always had scalability
in mind. Therefore, I structured the project with several packages, so that
future development would not get cluttered. This is why you can see several
packages with only 1 or 2 classes in them. The idea is that during future
development, it will be intuitive where classes should be placed. Most of 
the package names are self-explanatory. The one package I should explain
however is the "abc" package. This stands for abstract base class and 
should contain all abstract classes that serve as a "base class" for other
classes.

Here is an image of the folder structure to give an overview:

![Project folder structure.](folder structure.png)

### Process
The process of developing was not consistent, and was developed in short
bursts of work hours. Most of the requirements were implemented,
done and polished within 3 days of the deadline for each part. This went 
well in the beginning, but was clearly not enough time for the last phase.
The first phases were finished to a satisfactory degree, however the quality
degraded towards the end because of this.

There was not much need for a plan, or an issue board since the project was
clearly stated and broken down into smaller parts. I did however use TODO
comments to keep track of places I wanted to get back to in the future.
This proved to be helpful in the very last stages of development where I
polished the end product.

Using a version control system proved to be quite useful too, as I could
revert changes if something terribly bad happened. I also took use of git's
branching feature towards the end of the project. I wanted to refactor
my battle simulation, so I decided to create a new branch so that I could 
choose to develop both the new battle simulation and fix the old 
battle simulation at the same time. This way I could test out two features
at the same time and see what worked best. I did not get that far however
and ended up only developing one battle simulation (which is fine).

### Reflection
Throughout the course of this project, I learned a lot about design patterns
and the importance of javadoc and unit tests. Clean javadoc made methods
crystal clear and well documented, resulting in better code understanding.
In the future, javadoc should always be written while the method is being/has
been developed so that the original context and idea behind the method is not 
lost. Unit tests saved me potential several hours of debugging and is clearly
something to be implemented early on in the development stages where possible.

I also gained a better understanding of memory references, and the inner
workings of java memory handling and threads. I can now understand and 
quickly see when two variables point to the same memory address - causing
problems down the line.

There are a few things I would have done differently next project, however.
I recommend reading the task and planning each "phase" of development ahead
of time. Although the first 2 phases went well, the third phase of 
development required a lot more work and effort. Even though this
phase came in a time of high work load in other subjects too, I should have
at least taken a look and tried to fit some work in the tight schedule.

In the end, the program finished somewhat similarly to the wireframe. A lot
of features were however dropped because of poor time management. The most
basic and required features were focused on instead. I also had problems 
fitting terrain selection into the battle editor page, since this requirement
was announced *after* the wireframe was created. Thus, the battle editor
ended up looking differently than the wireframe. The wireframe can be found
in the repository, named balsamIQ-wireframe.bmpr and can be opened using
the website BalsamIQ.com. The wireframe might also be available at:
https://balsamiq.cloud/shgfqro/pcl4roa/rA609

### Conclusion
In the end, this project was a very good way to gain a lot of experience 
in the java programming language. Good practices such as design patterns,
javadoc and unit testing were taught well and gave a very valuable and real
insight into how it is to actually program larger systems. Although the 
project was not completely finished to a satisfactory degree, I believe
I have learned enough, and have the capabilities, to create clean, intuitive,
and efficient code; given enough time.
